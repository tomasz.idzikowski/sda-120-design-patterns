package io.idziksda.designpatterns.zadania;

import java.util.HashMap;
import java.util.Map;

public abstract class ComputerBuilder {
    protected Map<Part, Type> elements = new HashMap<>();

    public Computer build() {
        addPowerSupply();
        addProcessor();
        addMotherboard();
        addRam();
        return new Computer(elements);
    }

    private void addPowerSupply() {
        System.out.println("Charger has been added");
    }

    protected abstract void addMotherboard();

    protected abstract void addProcessor();

    protected abstract void addRam();

    @Override
    public String toString() {
        return "ComputerBuilder{" +
                "elements=" + elements +
                '}';
    }
}
