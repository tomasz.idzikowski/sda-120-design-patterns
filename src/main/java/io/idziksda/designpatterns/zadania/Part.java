package io.idziksda.designpatterns.zadania;

public enum Part {
    PROCESSOR,
    MOTHERBOARD,
    MEMORY
}
