package io.idziksda.designpatterns.zadania;

public class RichComputer extends ComputerBuilder{
    @Override
    protected void addMotherboard() {
        elements.put(Part.MOTHERBOARD,Type.RICH);
    }

    @Override
    protected void addProcessor() {
        elements.put(Part.PROCESSOR,Type.RICH);
    }


    @Override
    protected void addRam() {
        elements.put(Part.MEMORY,Type.ECONOMIC);
    }
}
