package io.idziksda.designpatterns.zadania;

public class Main {
    public static void main(String[] args) {
        ComputerBuilder richComputer=new RichComputer();
        richComputer.build();
        System.out.println(richComputer);
    }
}
