package io.idziksda.designpatterns.zadania;

public class EconomicComputer extends ComputerBuilder{
    @Override
    protected void addMotherboard() {
        elements.put(Part.MOTHERBOARD,Type.ECONOMIC);
    }

    @Override
    protected void addProcessor() {
        elements.put(Part.PROCESSOR,Type.ECONOMIC);
    }


    @Override
    protected void addRam() {
        elements.put(Part.MEMORY,Type.ECONOMIC);
    }
}
