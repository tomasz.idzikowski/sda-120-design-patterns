package io.idziksda.designpatterns.before.singleton;

public class Civilization {
    private int MAX_UNITS = 100;

    public void runGame() {
        System.out.println("Game loaded");
    }
}
