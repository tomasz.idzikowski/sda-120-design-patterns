package io.idziksda.designpatterns.before.factorymethod;

public class Muton extends Alien{

    public Muton(String rank, int stamina) {
        super(rank, stamina);
    }
}
