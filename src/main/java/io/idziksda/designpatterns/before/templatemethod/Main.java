package io.idziksda.designpatterns.before.templatemethod;

public class Main {
    public static void main(String[] args) {
        ApplicationWindowsBuilder appWindow = new ApplicationWindowsBuilder();
        ApplicationLinuxBuilder appLinux = new ApplicationLinuxBuilder();
        appWindow.writeApp();
        appLinux.writeApp();
    }
}
