package io.idziksda.designpatterns.before.observer;

public class TvNotification {
    public void updatePlayer(Player player) {
        System.out.println("TV : ");
        System.out.println(player + " " + player.getName());
        System.out.println("changed status to " + player.getStatus());
    }
}
