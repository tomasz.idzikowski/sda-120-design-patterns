package io.idziksda.designpatterns.before.observer;

public class Main {
    public static void main(String[] args) {
        Player lewy = new Player("Lewandowski", Status.IDLE);
        System.out.println("Beginning of the game");
        System.out.println(lewy);
        TvNotification tv = new TvNotification();
        RadioNotification radio = new RadioNotification();
        lewy.setStatus(Status.PASSED);
        tv.updatePlayer(lewy);
        radio.updatePlayer(lewy);
        lewy.setStatus(Status.SCORED);
        tv.updatePlayer(lewy);
        radio.updatePlayer(lewy);
        lewy.setStatus(Status.TACKLED);
        tv.updatePlayer(lewy);
        radio.updatePlayer(lewy);
    }
}
