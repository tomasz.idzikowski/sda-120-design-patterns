package io.idziksda.designpatterns.before.observer;

public enum Status {
    PASSED,
    TACKLED,
    SCORED,
    IDLE
}
