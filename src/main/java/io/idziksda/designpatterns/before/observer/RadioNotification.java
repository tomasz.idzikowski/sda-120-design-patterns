package io.idziksda.designpatterns.before.observer;

public class RadioNotification {
    public void updatePlayer(Player player) {
        System.out.println("Radio : ");
        System.out.println(player + " " + player.getName());
        System.out.println("changed status to " + player.getStatus());
    }
}
