package io.idziksda.designpatterns.after.factorymethod.units;

public class Sectoid extends Alien {
    Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }
}
