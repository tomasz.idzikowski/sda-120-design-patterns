package io.idziksda.designpatterns.after.factorymethod.units;

public class Muton extends Alien{
    Muton(String rank, int stamina) {
        super(rank, stamina);
    }
}
