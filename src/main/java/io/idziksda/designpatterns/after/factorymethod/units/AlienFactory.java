package io.idziksda.designpatterns.after.factorymethod.units;

public class AlienFactory extends UnitFactory{
    @Override
    public Alien create(String type) {
        switch (type) {
            case "sectoid":
                return new Sectoid("leader", 150);
            case "muton":
                return new Muton("soldier", 100);
            default:
                return new Sectoid("soldier",100);
        }
    }
}
