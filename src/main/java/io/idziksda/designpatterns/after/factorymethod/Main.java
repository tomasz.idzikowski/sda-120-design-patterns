package io.idziksda.designpatterns.after.factorymethod;

import io.idziksda.designpatterns.after.factorymethod.units.Alien;
import io.idziksda.designpatterns.after.factorymethod.units.AlienFactory;
import io.idziksda.designpatterns.after.factorymethod.units.Sectoid;
import io.idziksda.designpatterns.after.factorymethod.units.UnitFactory;

public class Main {
    public static void main(String[] args) {
        UnitFactory factory=new AlienFactory();
        Alien sectoid=factory.create("sectoid");
        Alien muton=factory.create("muton");
        System.out.println(sectoid);
        System.out.println(muton);
    }
}
