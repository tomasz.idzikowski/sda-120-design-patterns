package io.idziksda.designpatterns.after.abstractfactory.units;

public class Terminator extends Android {
    public Terminator(int version, int stamina) {
        super(version, stamina);
    }
}
