package io.idziksda.designpatterns.after.abstractfactory.units;

public abstract class UnitFactory {
    abstract public Alien createAlien(String type);
    abstract public Android createAndroid(String type);
}
