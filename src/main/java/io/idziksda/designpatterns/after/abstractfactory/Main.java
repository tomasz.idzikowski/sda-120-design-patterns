package io.idziksda.designpatterns.after.abstractfactory;

import io.idziksda.designpatterns.after.abstractfactory.units.BlueArmyFactory;
import io.idziksda.designpatterns.after.abstractfactory.units.RedArmyFactory;
import io.idziksda.designpatterns.after.abstractfactory.units.UnitFactory;

public class Main {
    public static void main(String[] args) {
        UnitFactory redArmyFactory = new RedArmyFactory();
        UnitFactory blueArmyFactory = new BlueArmyFactory();

        System.out.println(blueArmyFactory.createAndroid("T100"));
        System.out.println(blueArmyFactory.createAlien("sectoid"));

        System.out.println(redArmyFactory.createAndroid("T800"));
        System.out.println(redArmyFactory.createAlien("muton"));
    }
}
