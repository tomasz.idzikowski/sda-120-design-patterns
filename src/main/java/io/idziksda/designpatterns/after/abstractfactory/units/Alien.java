package io.idziksda.designpatterns.after.abstractfactory.units;

public abstract class Alien {
    private String rank;
    private int stamina;

    protected Alien(String rank, int stamina) {
        this.rank = rank;
        this.stamina = stamina;
    }

    @Override
    public String toString() {
        return "Alien{" +
                "rank='" + rank + '\'' +
                ", stamina=" + stamina +
                '}';
    }
}
