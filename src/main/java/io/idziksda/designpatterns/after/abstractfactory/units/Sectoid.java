package io.idziksda.designpatterns.after.abstractfactory.units;

public class Sectoid extends Alien {
    Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }
}
