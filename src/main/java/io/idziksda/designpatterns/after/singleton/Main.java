package io.idziksda.designpatterns.after.singleton;

public class Main {
    public static void main(String[] args) {
        Civilization game1=Civilization.getGame();
        Civilization game2=Civilization.getGame();
        System.out.println(game1.equals(game2));
    }
}
