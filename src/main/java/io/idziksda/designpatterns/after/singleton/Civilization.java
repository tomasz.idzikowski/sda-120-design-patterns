package io.idziksda.designpatterns.after.singleton;

public class Civilization {
    private static Civilization game;
    private int MAX_UNITS = 100;

    public void runGame() {
        System.out.println("Game loaded");
    }

    private Civilization() {
    }

    public static Civilization getGame() {
        if (game == null) {
            game = new Civilization();
        }
        return game;
    }
}
