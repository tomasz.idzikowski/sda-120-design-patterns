package io.idziksda.designpatterns.after.observer;

public enum Status {
    PASSED,
    TACKLED,
    SCORED,
    IDLE
}
