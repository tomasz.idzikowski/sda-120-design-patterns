package io.idziksda.designpatterns.after.observer;

public class TvNotification implements Observer{

    @Override
    public void update(Player player) {
        System.out.println("Tv info : ");
        System.out.println("Player " + player + " changed status to " + player.getStatus());
    }
}
