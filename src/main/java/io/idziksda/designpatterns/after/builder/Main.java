package io.idziksda.designpatterns.after.builder;

public class Main {
    public static void main(String[] args) {
        Computer computer=new Computer.ComputerBuilder()
                .buildMemory("16GB")
                .buildProcessor("Intel")
                .build();
        System.out.println(computer.toString());
    }
}
