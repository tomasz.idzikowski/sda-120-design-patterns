package io.idziksda.designpatterns.after.strategy;

public class Main {
    public static void main(String[] args) {
        Boxer rocky=new Boxer("Rocky");
        rocky.setPunch(new LeftPunch());
        rocky.hitOpponent();
    }
}
