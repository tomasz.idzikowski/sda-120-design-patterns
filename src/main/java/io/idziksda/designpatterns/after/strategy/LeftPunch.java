package io.idziksda.designpatterns.after.strategy;

public class LeftPunch implements Punch{
    @Override
    public void hit() {
        System.out.println("Hitting with left punch");
    }
}
