package io.idziksda.designpatterns.after.strategy;

public interface Punch {
    void hit();
}
