package io.idziksda.designpatterns.after.flyweight;

public class Car {
    private int mileage;
    private Color color;

    public Car(int mileage, String color) {
        this.mileage = mileage;
        switch (color) {
            case "black":
                this.color = ColorValues.getBlack();
                break;
            case "white":
                this.color = ColorValues.getWhite();
                break;
            case "yellow":
                this.color = ColorValues.getYellow();
                break;
            default:
                this.color = ColorValues.getGreen();
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "mileage=" + mileage +
                ", color=" + color +
                '}';
    }
}
