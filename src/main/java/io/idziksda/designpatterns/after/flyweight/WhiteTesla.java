package io.idziksda.designpatterns.after.flyweight;

public class WhiteTesla extends Car{

    public WhiteTesla(int mileage) {
        super(mileage, "white");
    }
}
