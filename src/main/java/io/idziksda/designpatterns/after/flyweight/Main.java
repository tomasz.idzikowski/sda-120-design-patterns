package io.idziksda.designpatterns.after.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Object> list=new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            list.add(new BlackDodge(10000));
        }
        list.forEach(car-> System.out.println(car));

        WhiteTesla whiteTesla=new WhiteTesla(40000);
        System.out.println(whiteTesla);
    }
}
