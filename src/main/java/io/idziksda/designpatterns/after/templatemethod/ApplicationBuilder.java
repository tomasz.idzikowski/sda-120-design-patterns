package io.idziksda.designpatterns.after.templatemethod;

public abstract class ApplicationBuilder {
    public void writeApp(){
        openSystem();
        installIDE();
        runIDE();
    }

    protected void openSystem(){
        System.out.println("Pressed power button. Your OS is starting");
    }

    abstract void installIDE();
    abstract void runIDE();
}
