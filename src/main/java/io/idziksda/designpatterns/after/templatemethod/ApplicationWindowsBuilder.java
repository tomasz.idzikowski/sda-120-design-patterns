package io.idziksda.designpatterns.after.templatemethod;

public class ApplicationWindowsBuilder extends ApplicationBuilder {
    @Override
    protected void installIDE() {
        System.out.println("Installing on Windows");
    }

    @Override
    protected void runIDE() {
        System.out.println("Running on Windows");
    }
}
